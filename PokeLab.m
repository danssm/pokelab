function varargout = PokeLab(varargin)
% POKELAB MATLAB code for PokeLab.fig
%      POKELAB, by itself, creates a new POKELAB or raises the existing
%      singleton*.
%
%      H = POKELAB returns the handle to a new POKELAB or the handle to
%      the existing singleton*.
%
%      POKELAB('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in POKELAB.M with the given input arguments.
%
%      POKELAB('Property','Value',...) creates a new POKELAB or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before PokeLab_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PokeLab_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PokeLab

% Last Modified by GUIDE v2.5 29-Nov-2014 01:35:39

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @PokeLab_OpeningFcn, ...
                   'gui_OutputFcn',  @PokeLab_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before PokeLab is made visible.
function PokeLab_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PokeLab (see VARARGIN)

% Choose default command line output for PokeLab
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% TEXT annotations need an axes as parent so create an invisible axes which
% is as big as the figure
handles.laxis = axes('parent',hObject,'units','normalized','position',[0 0 1 1],'visible','off');
% Find all static text UICONTROLS whose 'Tag' starts with latex_
lbls = findobj(hObject,'-regexp','tag','latex_*');
for i=1:length(lbls)
      l = lbls(i);
      % Get current text, position and tag
      set(l,'units','normalized');
      s = get(l,'string');
      p = get(l,'position');
      t = get(l,'tag');
      % Remove the UICONTROL
      delete(l);
      % Replace it with a TEXT object 
      handles.(t) = text(p(1),p(2),s,'interpreter','latex');
end

funcao = 'f(x) = x^3 - 20';
set(handles.textFuncao, 'String', funcao);


% --- Outputs from this function are returned to the command line.
function varargout = PokeLab_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in ButBisseccao.
function ButBisseccao_Callback(hObject, eventdata, handles)
% hObject    handle to ButBisseccao (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in ButPosicaoFalsa.
function ButPosicaoFalsa_Callback(hObject, eventdata, handles)
% hObject    handle to ButPosicaoFalsa (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in ButPontoFixo.
function ButPontoFixo_Callback(hObject, eventdata, handles)
% hObject    handle to ButPontoFixo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in ButNewtonRaphson.
function ButNewtonRaphson_Callback(hObject, eventdata, handles)
% hObject    handle to ButNewtonRaphson (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in ButPokebola.
function ButPokebola_Callback(hObject, eventdata, handles)
% hObject    handle to ButPokebola (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function textFuncao_Callback(hObject, eventdata, handles)
% hObject    handle to textFuncao (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of textFuncao as text
%        str2double(get(hObject,'String')) returns contents of textFuncao as a double


% --- Executes during object creation, after setting all properties.
function textFuncao_CreateFcn(hObject, eventdata, handles)
% hObject    handle to textFuncao (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
